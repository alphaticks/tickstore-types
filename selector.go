package tickstore_types

import (
	"fmt"
	"io"
	"math"
	"strings"
	"sync"
	"time"
)

type queryRunner struct {
	sync.RWMutex
	err      error
	freq     uint64
	from     uint64
	to       uint64
	updating bool
	q        TickstoreQuery
	tags     map[uint64]map[string]string
}

func newQueryRunner() *queryRunner {
	return &queryRunner{
		err:      nil,
		freq:     0,
		from:     0,
		to:       0,
		updating: false,
		q:        nil,
		tags:     make(map[uint64]map[string]string),
	}
}

func (q *queryRunner) ShouldUpdate(freq, from, to uint64) bool {
	q.Lock()
	defer q.Unlock()
	if q.updating {
		// Query already updating
		return false
	}
	if q.err == nil && q.freq == freq && from >= q.from && to <= q.to {
		return false
	}
	q.updating = true
	return true
}

func (q *queryRunner) SuccessUpdate(qu TickstoreQuery, freq, from, to uint64) {
	q.Lock()
	q.err = nil
	q.q = qu
	q.freq = freq
	q.from = from
	q.to = to
	q.updating = false
	q.Unlock()
}

func (q *queryRunner) FailedUpdate(err error) {
	q.Lock()
	q.err = err
	q.q = nil
	q.updating = false
	q.Unlock()
}

func (q *queryRunner) Close() error {
	fmt.Println("GETTING LOCK FOR CLOSE")
	q.Lock()
	defer q.Unlock()
	fmt.Println("GOT LOCK")
	q.from = uint64(math.MaxUint64)
	q.to = 0
	q.freq = 0
	q.tags = make(map[uint64]map[string]string)
	q.err = nil
	if q.q != nil {
		fmt.Println("CLOSING QUERY")
		err := q.q.Close()
		fmt.Println("QUERY CLOSED")
		q.q = nil
		return err
	}
	return nil
}

func (q *queryRunner) Closed() bool {
	q.RLock()
	defer q.RUnlock()
	return q.q == nil
}

func (q *queryRunner) TryClose(qu TickstoreQuery) error {
	q.Lock()
	defer q.Unlock()
	if q.q == qu {
		q.from = uint64(math.MaxUint64)
		q.to = 0
		q.freq = 0
		q.tags = make(map[uint64]map[string]string)
		if q.q.Err() != nil && q.q.Err() != io.EOF {
			q.err = q.q.Err()
		} else {
			q.err = nil
		}
		err := q.q.Close()
		q.q = nil
		return err
	} else {
		return nil
	}
}

func (q *queryRunner) From() uint64 {
	q.RLock()
	defer q.RUnlock()
	return q.from
}

func (q *queryRunner) To() uint64 {
	q.RLock()
	defer q.RUnlock()
	return q.to
}

func (q *queryRunner) Err() error {
	q.RLock()
	defer q.RUnlock()
	return q.err
}

type Selector struct {
	fetchLock  sync.Mutex
	ctx        DataContext
	selector   string
	epoch      uint64
	q          *queryRunner
	from       uint64
	to         uint64
	Frequency  uint64
	objectType string
	timeout    time.Duration
}

func NewSelector(ctx DataContext, selector string, epoch uint64, timeout time.Duration) *Selector {
	return &Selector{
		ctx:       ctx,
		selector:  selector,
		epoch:     epoch,
		q:         newQueryRunner(),
		from:      uint64(math.MaxUint64),
		to:        0,
		Frequency: 0,
		timeout:   timeout,
	}
}

func (sel *Selector) WithObjectType(objectType string) {
	sel.objectType = objectType
}

func (sel *Selector) Close() error {
	return sel.q.Close()
}

func (sel *Selector) Closed() bool {
	return sel.q.Closed()
}

func (sel *Selector) Clear() {
	sel.q.Close()
	sel.fetchLock.Lock()
	defer sel.fetchLock.Unlock()
	sel.ctx.Clear()
	sel.from = uint64(math.MaxUint64)
	sel.to = 0
}

func (sel *Selector) From() uint64 {
	from := sel.q.From()
	if sel.from < from {
		return sel.from
	} else {
		return from
	}
}

func (sel *Selector) To() uint64 {
	to := sel.q.To()
	if sel.to > to {
		return sel.to
	} else {
		return to
	}
}

func (sel *Selector) Tags(ID uint64) map[string]string {
	if sel.q != nil {
		return sel.q.tags[ID]
	} else {
		return nil
	}
}

func (sel *Selector) Err() error {
	return sel.q.Err()
}

func (sel *Selector) Fetch(client TickstoreClient, from, to, frequency uint64, streaming bool, args map[string]string) {
	// Behavior:
	// See if fetch running first

	// TODO prevent clearing if earlier

	if !sel.q.ShouldUpdate(frequency, from, to) {
		return
	}

	if streaming {
		to = to * 10
	}

	// ShouldUpdate has marked query as updating
	go func() {
		_ = sel.q.Close()
		// The last query was closed, fetchLock should be released soon
		sel.fetchLock.Lock()
		if sel.Frequency != frequency || from < sel.from {
			sel.ctx.Clear()
			sel.from = uint64(math.MaxUint64)
			sel.to = 0
		} else {
			// If we have already fetched up until
			// You want to link query that is
			from = sel.to
		}

		selector := sel.selector
		for k, v := range args {
			selector = strings.Replace(selector, "{"+k+"}", v, -1)
		}
		fmt.Println("SELECTOR", selector)
		qs := NewQuerySettings(
			WithStreaming(streaming),
			WithFrom(from),
			WithTo(to),
			WithTimeout(sel.timeout),
			WithSelector(selector))

		if sel.objectType != "" {
			qs.WithObjectType(sel.objectType)
		}

		if frequency > 0 {
			qs.Sampler = TickSampler{Interval: frequency}
		}

		sel.Frequency = frequency
		sel.from = from
		sel.to = from

		// TODO TickObject
		q, err := client.NewQuery(qs)
		if err != nil {
			// TODO log error
			// TODO handle ???
			fmt.Println("failed update", err)
			sel.q.FailedUpdate(err)
			sel.fetchLock.Unlock()
			return
		}
		sel.q.SuccessUpdate(q, frequency, from, to)

		ctx := make(map[string]interface{})
		ctx["frequency"] = frequency
		for q.Err() == nil {
			deadline := time.Now().Add(100 * time.Millisecond)
			q.SetNextDeadline(deadline)
			for q.Next() {
				tick, obj, groupID := q.Read()
				if tick > sel.to {
					sel.to = tick
				}
				if _, ok := sel.q.tags[groupID]; !ok {
					sel.q.tags[groupID] = q.Tags()
				}
				//ctx.inRange = (tick >= this._window[0] && tick <= this._window[1])
				if frequency > 0 {
					tick = (tick / frequency) * frequency
				}
				ticke := int64(tick) - int64(sel.epoch)
				sel.ctx.Apply(ticke, int64(frequency), obj, groupID, sel.q.tags[groupID])
				q.SetNextDeadline(deadline)
			}
		}
		sel.to = to

		// Fetching done
		sel.fetchLock.Unlock()

		// check that our query hasn't been replaced already by another goroutine
		_ = sel.q.TryClose(q)
	}()
}
