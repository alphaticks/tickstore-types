package tickobjects

import (
	"bytes"
	"encoding/binary"
	"reflect"
	"unsafe"

	"github.com/melaurent/gotickfile/v2"
)

type OHLCV struct {
	o         float32
	h         float32
	l         float32
	c         float32
	buyCount  uint32
	sellCount uint32
	buyVol    float32
	sellVol   float32
	price     float32
}

type OHLCVDelta struct {
	o         float32
	h         float32
	l         float32
	c         float32
	buyCount  uint32
	sellCount uint32
	buyvol    float32
	sellvol   float32
	price     float32
}

var OHLCVDeltaSize = reflect.TypeOf(OHLCVDelta{}).Size()

func (p *OHLCV) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, p.o)
	_ = binary.Write(w, binary.LittleEndian, p.h)
	_ = binary.Write(w, binary.LittleEndian, p.l)
	_ = binary.Write(w, binary.LittleEndian, p.c)
	_ = binary.Write(w, binary.LittleEndian, p.buyCount)
	_ = binary.Write(w, binary.LittleEndian, p.sellCount)
	_ = binary.Write(w, binary.LittleEndian, p.buyVol)
	_ = binary.Write(w, binary.LittleEndian, p.sellVol)
	_ = binary.Write(w, binary.LittleEndian, p.price)
	return w.Bytes()
}

func (p *OHLCV) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &p.o); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.h); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.l); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.c); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.buyCount); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.sellCount); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.buyVol); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.sellVol); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &p.price); err != nil {
		return err
	}
	return nil
}

func (p *OHLCV) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	obj := other.(*OHLCV)
	val := OHLCVDelta{
		o:         obj.o,
		h:         obj.h,
		l:         obj.l,
		c:         obj.c,
		buyCount:  obj.buyCount,
		sellCount: obj.sellCount,
		buyvol:    obj.buyVol,
		sellvol:   obj.sellVol,
		price:     obj.price,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (p *OHLCV) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltass[len(deltass)-1]
}

func (p *OHLCV) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	obj := (*OHLCVDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*OHLCVDeltaSize))
	p.o = obj.o
	p.h = obj.h
	p.l = obj.l
	p.c = obj.c
	p.buyCount = obj.buyCount
	p.sellCount = obj.sellCount
	p.buyVol = obj.buyvol
	p.sellVol = obj.sellvol
	p.price = obj.price

	return nil
}

func (p *OHLCV) Clone() TickObject {
	return &OHLCV{
		o:         p.o,
		h:         p.h,
		l:         p.l,
		c:         p.c,
		buyCount:  p.buyCount,
		sellCount: p.sellCount,
		buyVol:    p.buyVol,
		sellVol:   p.sellVol,
		price:     p.price,
	}
}

func (p *OHLCV) Float64() float64 {
	return float64(p.c)
}

func (p *OHLCV) Open() float32 {
	return p.o
}

func (p *OHLCV) High() float32 {
	return p.h
}

func (p *OHLCV) Low() float32 {
	return p.l
}

func (p *OHLCV) Close() float32 {
	return p.c
}

func (p *OHLCV) BuyVol() float32 {
	return p.buyVol
}

func (p *OHLCV) SellVol() float32 {
	return p.sellVol
}

func (p *OHLCV) Price() float32 {
	return p.price
}

func (p *OHLCV) BuyCount() uint32 {
	return p.buyCount
}

func (p *OHLCV) SellCount() uint32 {
	return p.sellCount
}

type SellVol struct {
	Amount float64 `json:"amount"`
}

func (t *SellVol) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.Amount)
	return w.Bytes()
}

func (t *SellVol) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.Amount); err != nil {
		return err
	}
	return nil
}

func (t *SellVol) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*SellVol).Amount
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *SellVol) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltass[len(deltass)-1]
}

func (t *SellVol) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	bb := (*SellVol)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))
	t.Amount = bb.Amount
	return nil
}

func (t *SellVol) Clone() TickObject {
	return &SellVol{
		Amount: t.Amount,
	}
}

func (t *SellVol) Float64() float64 {
	return t.Amount
}

type BuyVol struct {
	Amount float64 `json:"amount"`
}

func (t *BuyVol) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.Amount)
	return w.Bytes()
}

func (t *BuyVol) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.Amount); err != nil {
		return err
	}
	return nil
}

func (t *BuyVol) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*BuyVol).Amount
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *BuyVol) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltass[len(deltass)-1]
}

func (t *BuyVol) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	bb := (*BuyVol)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))
	t.Amount = bb.Amount
	return nil
}

func (t *BuyVol) Clone() TickObject {
	return &BuyVol{
		Amount: t.Amount,
	}
}

func (t *BuyVol) Float64() float64 {
	return t.Amount
}
