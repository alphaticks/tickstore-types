package tickobjects

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"reflect"
	"unsafe"
)

type TradeDelta struct {
	Price float64 `json:"price"`
	Size  float64 `json:"size"`
}

var TradeDeltaSize = reflect.TypeOf(TradeDelta{}).Size()

type Trade struct {
	price float64
	size  float64
}

func NewTrade(price, size float64) *Trade {
	return &Trade{price: price, size: size}
}

func (t *Trade) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.price)
	_ = binary.Write(w, binary.LittleEndian, t.size)
	return w.Bytes()
}

func (t *Trade) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.price); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &t.size); err != nil {
		return err
	}
	return nil
}

func (t *Trade) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	otherTrd := other.(*Trade)
	val := TradeDelta{
		Price: otherTrd.price,
		Size:  otherTrd.size,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *Trade) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	var aggDeltas []TradeDelta
	for _, deltasTmp := range deltass {
		for i := 0; i < deltasTmp.Len; i++ {
			delta := *(*TradeDelta)(unsafe.Pointer(uintptr(deltasTmp.Pointer) + uintptr(i)*TradeDeltaSize))
			aggDeltas = append(aggDeltas, delta)
		}
	}

	var ptr unsafe.Pointer
	if len(aggDeltas) > 0 {
		ptr = unsafe.Pointer(&aggDeltas[0])
	}
	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     len(aggDeltas),
	}
}

func (t *Trade) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	op := (*TradeDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*TradeDeltaSize))
	t.size = op.Size
	t.price = op.Price
	return nil
}

func (t *Trade) Clone() TickObject {
	return &Trade{
		price: t.price,
		size:  t.size,
	}
}

func (t *Trade) Price() float64 {
	return t.price
}

func (t *Trade) Size() float64 {
	return t.size
}

type BuyTrade struct {
	price float64
	size  float64
}

func NewBuyTrade(price, size float64) *BuyTrade {
	return &BuyTrade{price: price, size: size}
}

func (t *BuyTrade) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.price)
	_ = binary.Write(w, binary.LittleEndian, t.size)
	return w.Bytes()
}

func (t *BuyTrade) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.price); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &t.size); err != nil {
		return err
	}
	return nil
}

func (t *BuyTrade) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	otherTrd := other.(*BuyTrade)
	val := TradeDelta{
		Price: otherTrd.price,
		Size:  otherTrd.size,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *BuyTrade) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	var aggDeltas []TradeDelta
	for _, deltasTmp := range deltass {
		for i := 0; i < deltasTmp.Len; i++ {
			delta := *(*TradeDelta)(unsafe.Pointer(uintptr(deltasTmp.Pointer) + uintptr(i)*TradeDeltaSize))
			aggDeltas = append(aggDeltas, delta)
		}
	}

	var ptr unsafe.Pointer
	if len(aggDeltas) > 0 {
		ptr = unsafe.Pointer(&aggDeltas[0])
	}
	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     len(aggDeltas),
	}
}

func (t *BuyTrade) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	op := (*TradeDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*TradeDeltaSize))
	t.size = op.Size
	t.price = op.Price
	return nil
}

func (t *BuyTrade) Clone() TickObject {
	return &BuyTrade{
		price: t.price,
		size:  t.size,
	}
}

func (t *BuyTrade) Price() float64 {
	return t.price
}

func (t *BuyTrade) Size() float64 {
	return t.size
}

type SellTrade struct {
	price float64
	size  float64
}

func (t *SellTrade) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.price)
	_ = binary.Write(w, binary.LittleEndian, t.size)
	return w.Bytes()
}

func (t *SellTrade) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.price); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &t.size); err != nil {
		return err
	}
	return nil
}

func (t *SellTrade) Clone() TickObject {
	return &SellTrade{
		price: t.price,
		size:  t.size,
	}
}

func (t *SellTrade) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	otherTrd := other.(*SellTrade)
	val := TradeDelta{
		Price: otherTrd.price,
		Size:  otherTrd.size,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *SellTrade) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	var aggDeltas []TradeDelta
	for _, deltasTmp := range deltass {
		for i := 0; i < deltasTmp.Len; i++ {
			delta := *(*TradeDelta)(unsafe.Pointer(uintptr(deltasTmp.Pointer) + uintptr(i)*TradeDeltaSize))
			aggDeltas = append(aggDeltas, delta)
		}
	}

	var ptr unsafe.Pointer
	if len(aggDeltas) > 0 {
		ptr = unsafe.Pointer(&aggDeltas[0])
	}
	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     len(aggDeltas),
	}
}

func (t *SellTrade) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	op := (*TradeDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*TradeDeltaSize))
	t.size = op.Size
	t.price = op.Price
	return nil
}

func (t *SellTrade) Price() float64 {
	return t.price
}

func (t *SellTrade) Size() float64 {
	return t.size
}

type TradePrice struct {
	Price float64
}

func (t *TradePrice) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.Price)
	return w.Bytes()
}

func (t *TradePrice) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.Price); err != nil {
		return err
	}
	return nil
}

func (t *TradePrice) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*TradePrice).Price
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *TradePrice) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltass[len(deltass)-1]
}

func (t *TradePrice) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	bb := (*TradePrice)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))
	t.Price = bb.Price
	return nil
}

func (t *TradePrice) Clone() TickObject {
	return &TradePrice{
		Price: t.Price,
	}
}

func (t *TradePrice) Float64() float64 {
	return t.Price
}

type TradeAmount struct {
	Amount float64 `json:"amount"`
}

func (t *TradeAmount) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, t.Amount)
	return w.Bytes()
}

func (t *TradeAmount) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &t.Amount); err != nil {
		return err
	}
	return nil
}

func (t *TradeAmount) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*TradeAmount).Amount
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (t *TradeAmount) AggregateDeltas(deltass []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltass[len(deltass)-1]
}

func (t *TradeAmount) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	bb := (*TradeAmount)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))
	t.Amount = bb.Amount
	return nil
}

func (t *TradeAmount) Clone() TickObject {
	return &TradeAmount{
		Amount: t.Amount,
	}
}

func (t *TradeAmount) Float64() float64 {
	return t.Amount
}

func (t *TradeAmount) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[%f]", t.Amount)), nil
}

type TradeObject interface {
	Price() float64
	Size() float64
}
