package tickobjects

import (
	"bytes"
	"encoding/gob"
	"github.com/melaurent/gotickfile/v2"
	"reflect"
	"unsafe"
)

type ETHBlock struct {
	number     uint64
	time       uint64
	size       float64
	baseFee    [32]byte
	difficulty [32]byte
	gasLimit   uint64
	gasUsed    uint64
}

type ETHBlockSnapshot struct {
	Number     uint64
	Time       uint64
	Size       float64
	BaseFee    [32]byte
	Difficulty [32]byte
	GasLimit   uint64
	GasUsed    uint64
}

func NewETHBlock() *ETHBlock {
	return &ETHBlock{
		number:     0,
		time:       0,
		size:       0,
		baseFee:    [32]byte{},
		difficulty: [32]byte{},
		gasLimit:   0,
		gasUsed:    0,
	}
}

func (bl *ETHBlock) ToSnapshot() []byte {
	buf := bytes.NewBuffer(nil)
	enc := gob.NewEncoder(buf)
	snap := ETHBlockSnapshot{
		Number:     bl.number,
		Time:       bl.time,
		Size:       bl.size,
		BaseFee:    bl.baseFee,
		Difficulty: bl.difficulty,
		GasLimit:   bl.gasLimit,
		GasUsed:    bl.gasUsed,
	}

	if err := enc.Encode(snap); err != nil {
		panic(err)
	}
	return buf.Bytes()
}

func (bl *ETHBlock) FromSnapshot(b []byte) error {
	buf := bytes.NewReader(b)
	dec := gob.NewDecoder(buf)
	snap := ETHBlockSnapshot{}

	if err := dec.Decode(&snap); err != nil {
		panic(err)
	}

	bl.number = snap.Number
	bl.time = snap.Time
	bl.size = snap.Size
	bl.baseFee = snap.BaseFee
	bl.difficulty = snap.Difficulty
	bl.gasLimit = snap.GasLimit
	bl.gasUsed = snap.GasUsed
	return nil
}

func (bl *ETHBlock) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	o := other.(*ETHBlock)
	val := ETHBlockDelta{
		Number:     o.number,
		Time:       o.time,
		Size:       o.size,
		BaseFee:    o.baseFee,
		Difficulty: o.difficulty,
		GasLimit:   o.gasLimit,
		GasUsed:    o.gasUsed,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (bl *ETHBlock) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (bl *ETHBlock) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	size := reflect.TypeOf(ETHBlockDelta{}).Size()
	ptr := uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*size
	op := (*ETHBlockDelta)(unsafe.Pointer(ptr))
	bl.number = op.Number
	bl.time = op.Time
	bl.size = op.Size
	bl.baseFee = op.BaseFee
	bl.difficulty = op.Difficulty
	bl.gasLimit = op.GasLimit
	bl.gasUsed = op.GasUsed
	return nil
}

func (bl *ETHBlock) Clone() TickObject {
	var baseFee [32]byte
	var difficulty [32]byte
	copy(baseFee[:], bl.baseFee[:])
	copy(difficulty[:], bl.difficulty[:])
	return &ETHBlock{
		number:     bl.number,
		time:       bl.time,
		size:       bl.size,
		baseFee:    bl.baseFee,
		difficulty: bl.difficulty,
		gasLimit:   bl.gasLimit,
		gasUsed:    bl.gasUsed,
	}
}

func (bl *ETHBlock) Number() uint64 {
	return bl.number
}

func (bl *ETHBlock) Time() uint64 {
	return bl.time
}

func (bl *ETHBlock) Size() float64 {
	return bl.size
}

func (bl *ETHBlock) BaseFee() [32]byte {
	return bl.baseFee
}

func (bl *ETHBlock) Difficulty() [32]byte {
	return bl.difficulty
}

func (bl *ETHBlock) GasLimit() uint64 {
	return bl.gasLimit
}
func (bl *ETHBlock) GasUsed() uint64 {
	return bl.gasUsed
}

type ETHBlockDelta struct {
	Number     uint64
	Time       uint64
	Size       float64
	BaseFee    [32]byte
	Difficulty [32]byte
	GasLimit   uint64
	GasUsed    uint64
}

type ETHBlockDeltaPacked struct {
	All [13]uint64
}

func NewETHBlockDelta(number, time uint64, size float64, baseFee, difficulty [32]byte, gasLimit, gasUsed uint64) ETHBlockDelta {
	return ETHBlockDelta{
		Number:     number,
		Time:       time,
		Size:       size,
		BaseFee:    baseFee,
		Difficulty: difficulty,
		GasLimit:   gasLimit,
		GasUsed:    gasUsed,
	}
}
