package tickobjects

import (
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"reflect"
)

var (
	objectTypes = make(map[string]reflect.Type) // Contains a mapping from type ID to tick object type
	deltaTypes  = make(map[string]reflect.Type) // Contains a mapping from type ID to tick delta type

	Measurements = make(map[string]string)
)

func RegisterMeasurement(name string, typeID string) error {
	if m, ok := Measurements[name]; ok {
		_, ok := objectTypes[typeID]

		if !ok {
			return fmt.Errorf("unknown type %s", typeID)
		}
		if m != typeID {
			return fmt.Errorf("measurement already exists")
		} else {
			return nil
		}
	} else {
		_, ok := objectTypes[typeID]
		if !ok {
			return fmt.Errorf("unknown type %s", typeID)
		}
		Measurements[name] = typeID
	}

	return nil
}

func RegisterTickFunctor(name string, functorType reflect.Type, deltaType reflect.Type) error {
	_, ok := reflect.New(functorType).Interface().(TickFunctor)
	if !ok {
		return fmt.Errorf("type %s does not implement TickFunctor", name)
	}
	objectTypes[name] = functorType
	deltaTypes[name] = deltaType

	return nil
}

func RegisterTickObject(name string, objectType reflect.Type, deltaType reflect.Type) error {
	objectTypes[name] = objectType
	deltaTypes[name] = deltaType

	return nil
}

func GetMeasurement(name string) (reflect.Type, reflect.Type, bool) {
	if typ, ok := Measurements[name]; ok {
		return objectTypes[typ], deltaTypes[typ], true
	} else {
		return nil, nil, false
	}
}

func GetTickObject(name string) (reflect.Type, reflect.Type, bool) {
	objTyp, ok := objectTypes[name]
	dltTyp, ok := deltaTypes[name]
	return objTyp, dltTyp, ok
}

type TickObject interface {
	ToSnapshot() []byte
	FromSnapshot([]byte) error
	DeltasTo(TickObject) (gotickfile.TickDeltas, error)
	AggregateDeltas([]gotickfile.TickDeltas) gotickfile.TickDeltas
	ProcessDeltas(gotickfile.TickDeltas) error
	Clone() TickObject
}

type TickFunctor interface {
	TickObject
	Temporal() bool
	Progress(uint64) (*gotickfile.TickDeltas, error)
	ApplySnapshot([]byte, uint64, uint64) (*gotickfile.TickDeltas, error)
	ApplyDeltas(gotickfile.TickDeltas, uint64, uint64) (*gotickfile.TickDeltas, error)
	Initialize(TickFunctor, []string) error
}

type Int64Object interface {
	TickObject
	Int64() int64
}

type UInt64Object interface {
	TickObject
	UInt64() uint64
}

type Float64Object interface {
	TickObject
	Float64() float64
}

type Float64SliceObject interface {
	TickObject
	Float64Slice() []float64
}

type OBLiquidityObject interface {
	TickFunctor
	GetOBPrice(depth float64, cutoff *float64) (float64, float64, error)
	GetEWOBPrice(depth, exponent float64) (float64, float64, error)
	BidDepth(depth float64, quote bool) float64
	AskDepth(depth float64, quote bool) float64
	Frozen() bool
}

type BlockEventObject interface {
	TickObject
	BlockNumber() uint64
}

type Float64Functor interface {
	TickFunctor
	Float64() float64
}

type OHLCVObject interface {
	Open() float32
	High() float32
	Low() float32
	Close() float32
	SellCount() uint32
	BuyCount() uint32
	SellVol() float32
	BuyVol() float32
	Price() float32
}
