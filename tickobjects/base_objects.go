package tickobjects

import (
	"bytes"
	"encoding/binary"
	"github.com/melaurent/gotickfile/v2"
	"reflect"
	"unsafe"
)

func init() {
	if err := RegisterTickObject("Int64", reflect.TypeOf(Int64(0)), reflect.TypeOf(int64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("UInt64", reflect.TypeOf(UInt64(0)), reflect.TypeOf(uint64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64", reflect.TypeOf(Float64(0)), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
}

type Int64 int64

func (o *Int64) ToSnapshot() []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	n := binary.PutVarint(buf, int64(*o))
	return buf[:n]
}

func (o *Int64) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	v, err := binary.ReadVarint(r)
	if err != nil {
		return err
	}
	*o = Int64(v)

	return nil
}

func (o *Int64) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*Int64).Int64()
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Int64) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (o *Int64) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	*o = Int64(*(*int64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8))))
	return nil
}

func (o *Int64) Clone() TickObject {
	v := Int64(int64(*o))
	return &v
}

func (o *Int64) Int64() int64 {
	return int64(*o)
}

func (o *Int64) ApplySnapshot(b []byte, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.FromSnapshot(b); err != nil {
		return nil, err
	}
	val := o.Int64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Int64) ApplyDeltas(deltas gotickfile.TickDeltas, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.ProcessDeltas(deltas); err != nil {
		return nil, err
	}
	val := o.Int64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Int64) Progress(_ uint64) (*gotickfile.TickDeltas, error) {
	return nil, nil
}

func (o *Int64) Initialize(TickFunctor, []string) error {
	return nil
}

type UInt64 uint64

func (o *UInt64) ToSnapshot() []byte {
	buf := make([]byte, 8)
	binary.LittleEndian.PutUint64(buf, uint64(*o))
	return buf
}

func (o *UInt64) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	var val uint64
	err := binary.Read(r, binary.LittleEndian, &val)
	if err != nil {
		return err
	}
	*o = UInt64(val)

	return nil
}

func (o *UInt64) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*UInt64).UInt64()
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *UInt64) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (o *UInt64) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	*o = UInt64(*(*uint64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8))))
	return nil
}

func (o *UInt64) Clone() TickObject {
	v := UInt64(uint64(*o))
	return &v
}

func (o *UInt64) ApplySnapshot(b []byte, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.FromSnapshot(b); err != nil {
		return nil, err
	}
	val := o.UInt64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *UInt64) ApplyDeltas(deltas gotickfile.TickDeltas, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.ProcessDeltas(deltas); err != nil {
		return nil, err
	}
	val := o.UInt64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *UInt64) Progress(_ uint64) (*gotickfile.TickDeltas, error) {
	return nil, nil
}

func (o *UInt64) Initialize(TickFunctor, []string) error {
	return nil
}

func (o *UInt64) UInt64() uint64 {
	return uint64(*o)
}

type Float64 float64

func (o *Float64) ToSnapshot() []byte {
	buf := &bytes.Buffer{}
	snap := float64(*o)
	_ = binary.Write(buf, binary.LittleEndian, &snap)
	return buf.Bytes()
}

func (o *Float64) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	var v float64
	err := binary.Read(r, binary.LittleEndian, &v)
	if err != nil {
		return err
	}
	*o = Float64(v)

	return nil
}

func (o *Float64) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*Float64).Float64()
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Float64) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (o *Float64) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	*o = Float64(*(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8))))
	return nil
}

func (o *Float64) Clone() TickObject {
	v := Float64(float64(*o))
	return &v
}

func (o *Float64) ApplySnapshot(b []byte, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.FromSnapshot(b); err != nil {
		return nil, err
	}
	val := o.Float64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Float64) ApplyDeltas(deltas gotickfile.TickDeltas, _ uint64, _ uint64) (*gotickfile.TickDeltas, error) {
	if err := o.ProcessDeltas(deltas); err != nil {
		return nil, err
	}
	val := o.Float64()
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (o *Float64) Progress(_ uint64) (*gotickfile.TickDeltas, error) {
	return nil, nil
}

func (o *Float64) Initialize(TickFunctor, []string) error {
	return nil
}

func (o *Float64) Float64() float64 {
	return float64(*o)
}
