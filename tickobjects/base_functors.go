package tickobjects

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"math"
	"sort"
	"strconv"
	"unsafe"
)

type EMA struct {
	val      float64
	exponent float64
	obj      Float64Functor
}

func (p *EMA) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, p.val)
	return w.Bytes()
}

func (p *EMA) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &p.val); err != nil {
		return err
	}
	return nil
}

func (p *EMA) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	val := other.(*EMA).val
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (p *EMA) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (p *EMA) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	p.val = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (p *EMA) Clone() TickObject {
	var F Float64Functor
	if p.obj != nil {
		F = p.obj.Clone().(Float64Functor)
	}
	return &EMA{
		val:      p.val,
		exponent: p.exponent,
		obj:      F,
	}
}

// Functor part
func (p *EMA) ApplySnapshot(snap []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	_, err := p.obj.ApplySnapshot(snap, ID, tick)
	if err != nil {
		return nil, err
	}
	val := (p.exponent * p.obj.Float64()) + (1.-p.exponent)*p.val
	p.val = val
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (p *EMA) ApplyDelta(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	_, err := p.obj.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	val := (p.exponent * p.obj.Float64()) + (1.-p.exponent)*p.val
	p.val = val
	return &gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (p *EMA) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	return nil, nil
}

func (p *EMA) Initialize(functor TickFunctor, args []string) error {
	obj, ok := functor.(Float64Functor)
	if !ok {
		return fmt.Errorf("EMA expects a Float64Functor")
	}
	val, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		return err
	}
	p.obj = obj
	p.exponent = val

	return nil
}

func (p *EMA) Float64() float64 {
	return p.val
}

// The LastWrite functor is just a transparent functor that updates at
// every write to the instrument. It has not deltas, no snapshot,
// and do not reconstruct the object.

type LastWrite struct {
}

func (lw *LastWrite) ToSnapshot() []byte {
	return nil
}

func (lw *LastWrite) FromSnapshot(b []byte) error {
	return nil
}

func (lw *LastWrite) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	return gotickfile.TickDeltas{}, nil
}

func (lw *LastWrite) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return gotickfile.TickDeltas{}
}

func (lw *LastWrite) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	return nil
}

func (lw *LastWrite) Clone() TickObject {
	return &LastWrite{}
}

func (lw *LastWrite) ApplySnapshot(snapshot []byte, ID, tick uint64) (gotickfile.TickDeltas, error) {
	return gotickfile.TickDeltas{}, nil
}

func (lw *LastWrite) ApplyDeltas(deltas interface{}, ID, tick uint64) (gotickfile.TickDeltas, error) {
	return gotickfile.TickDeltas{}, nil
}

func (lw *LastWrite) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	return nil, nil
}

func (lw *LastWrite) Initialize(f TickFunctor, _ []string) error {
	return nil
}

type Float64WindowSum struct {
	F        TickFunctor
	freq     uint64
	nextTick uint64
	sum      float64
}

func (sum *Float64WindowSum) ToSnapshot() []byte {
	buff := &bytes.Buffer{}
	_ = binary.Write(buff, binary.LittleEndian, &sum.sum)
	return buff.Bytes()
}

func (sum *Float64WindowSum) FromSnapshot(snapshot []byte) error {
	reader := bytes.NewReader(snapshot)
	return binary.Read(reader, binary.LittleEndian, &sum.sum)
}

func (sum *Float64WindowSum) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64WindowSum).sum
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (sum *Float64WindowSum) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (sum *Float64WindowSum) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	sum.sum = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (sum *Float64WindowSum) Clone() TickObject {
	// TODO vals clone
	return &Float64WindowSum{
		sum:  sum.sum,
		F:    sum.F.Clone().(TickFunctor),
		freq: sum.freq,
	}
}

func (sum *Float64WindowSum) ApplySnapshot(snapshot []byte, ID, tick uint64) (tDeltas *gotickfile.TickDeltas, err error) {
	if sum.F != nil {
		udeltas, err := sum.F.ApplySnapshot(snapshot, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			if tick >= sum.nextTick {
				sum.sum = 0.
				sum.nextTick = ((tick / sum.freq) + 1) * sum.freq
			}
			sum.sum += sum.F.(Float64Object).Float64()
			val := sum.sum
			tDeltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	} else {
		if err := sum.FromSnapshot(snapshot); err != nil {
			return nil, err
		}
	}
	return
}

func (sum *Float64WindowSum) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (tDeltas *gotickfile.TickDeltas, err error) {
	if sum.F != nil {
		udeltas, err := sum.F.ApplyDeltas(deltas, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			if tick >= sum.nextTick {
				sum.sum = 0.
				sum.nextTick = ((tick / sum.freq) + 1) * sum.freq
			}
			sum.sum += sum.F.(Float64Object).Float64()
			val := sum.sum
			tDeltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	} else {
		if err := sum.ProcessDeltas(deltas); err != nil {
			return nil, err
		}
	}
	return
}

func (sum *Float64WindowSum) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (sum *Float64WindowSum) Initialize(f TickFunctor, args []string) error {
	if _, ok := f.(Float64Object); !ok {
		return fmt.Errorf("Float64WindowSum is expecting a Float64Object")
	}
	sum.F = f

	val, err := strconv.ParseUint(args[0], 10, 64)
	if err != nil {
		return err
	}
	sum.freq = val
	return nil
}

func (sum *Float64WindowSum) Float64() float64 {
	return sum.sum
}

type UInt64Sum struct {
	f    TickFunctor
	vals map[uint64]TickFunctor
	sum  uint64
}

func (sum *UInt64Sum) ToSnapshot() []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	n := binary.PutUvarint(buf, sum.sum)
	return buf[:n]
}

func (sum *UInt64Sum) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	v, err := binary.ReadUvarint(r)
	if err != nil {
		return err
	}
	sum.sum = v

	return nil
}

func (sum *UInt64Sum) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*UInt64Sum).sum
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (sum *UInt64Sum) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (sum *UInt64Sum) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	sum.sum = *(*uint64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (sum *UInt64Sum) Clone() TickObject {
	// TODO vals clone
	return &UInt64Sum{
		sum:  sum.sum,
		f:    sum.f.Clone().(TickFunctor),
		vals: make(map[uint64]TickFunctor),
	}
}

func (sum *UInt64Sum) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	var deltas interface{}

	// apply the snapshot to the corresponding tick object
	if val, ok := sum.vals[ID]; ok {

		oldVal := val.(UInt64Object).UInt64()

		var err error
		deltas, err = val.ApplySnapshot(snapshot, ID, tick)
		if err != nil {
			return nil, err
		}

		// Sample only if the underlying functor has changed
		if deltas != nil {
			// Remove old val from sum
			sum.sum -= oldVal

			// Add back new val
			sum.sum += sum.vals[ID].(UInt64Object).UInt64()
			// The produced deltas is just the new sum
			sumVal := sum.sum
			return &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&sumVal),
				Len:     1,
			}, nil
		} else {
			return nil, nil
		}

	} else {
		// Clone functor
		funct := sum.f.Clone().(TickFunctor)

		var err error
		deltas, err = funct.ApplySnapshot(snapshot, ID, tick)
		if err != nil {
			return nil, err
		}
		sum.vals[ID] = funct

		// New element so add it to sum
		sum.sum += sum.vals[ID].(UInt64Object).UInt64()
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	}
}

func (sum *UInt64Sum) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	// apply the delta to the corresponding tick object
	oldVal := sum.vals[ID].(UInt64Object).UInt64()

	udeltas, err := sum.vals[ID].ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	// Sample only if the underlying functor has changed
	if udeltas != nil {
		// Remove old val from sum
		sum.sum -= oldVal

		// Add back new val
		sum.sum += sum.vals[ID].(UInt64Object).UInt64()

		// The produced deltas is just the new sum
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (sum *UInt64Sum) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (sum *UInt64Sum) Initialize(f TickFunctor, _ []string) error {
	if _, ok := f.(UInt64Object); !ok {
		return fmt.Errorf("UInt64Sum is expecting a UInt64Object")
	}
	sum.f = f
	return nil
}

func (sum *UInt64Sum) UInt64() uint64 {
	return sum.sum
}

type Float64Average struct {
	f   TickFunctor
	avg float64
}

func (avg *Float64Average) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	binary.LittleEndian.PutUint64(b, math.Float64bits(avg.avg))
	return b
}

func (avg *Float64Average) FromSnapshot(b []byte) error {
	// TODO
	return nil
}

func (avg *Float64Average) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64Average).avg
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (avg *Float64Average) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (avg *Float64Average) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	avg.avg = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (avg *Float64Average) Clone() TickObject {
	// TODO vals clone
	return &Float64Average{
		avg: avg.avg,
		f:   avg.f.Clone().(TickFunctor),
	}
}

func (avg *Float64Average) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := avg.f.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		data := avg.f.(Float64SliceObject).Float64Slice()
		// No sample if empty array
		if len(data) == 0 {
			return nil, nil
		}
		var sum float64 = 0
		for _, d := range data {
			sum += d
		}
		sum /= float64(len(data))
		avg.avg = sum
		avgVal := avg.avg
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&avgVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (avg *Float64Average) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := avg.f.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		data := avg.f.(Float64SliceObject).Float64Slice()
		// No sample if empty array
		if len(data) == 0 {
			return nil, nil
		}
		var sum float64 = 0
		for _, d := range data {
			sum += d
		}
		sum /= float64(len(data))
		avg.avg = sum

		val := avg.avg
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (avg *Float64Average) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (avg *Float64Average) Initialize(f TickFunctor, _ []string) error {
	if _, ok := f.(Float64SliceObject); !ok {
		return fmt.Errorf("Float64Average is expecting a Float64SliceObject")
	}
	avg.f = f
	return nil
}

func (avg *Float64Average) Float64() float64 {
	return avg.avg
}

func (avg *Float64Average) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[%f]", avg.avg)), nil
}

type Float64Sum struct {
	f   TickFunctor
	sum float64
}

func (sum *Float64Sum) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	binary.LittleEndian.PutUint64(b, math.Float64bits(sum.sum))
	return b
}

func (sum *Float64Sum) FromSnapshot(b []byte) error {
	// TODO
	return nil
}

func (sum *Float64Sum) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64Sum).sum
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (sum *Float64Sum) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (sum *Float64Sum) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	sum.sum = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (sum *Float64Sum) Clone() TickObject {
	return &Float64Sum{
		sum: sum.sum,
		f:   sum.f.Clone().(TickFunctor),
	}
}

func (sum *Float64Sum) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	deltas, err := sum.f.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	if deltas != nil {
		data := sum.f.(Float64SliceObject).Float64Slice()
		sum.sum = 0
		for _, d := range data {
			sum.sum += d
		}
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (sum *Float64Sum) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := sum.f.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		data := sum.f.(Float64SliceObject).Float64Slice()
		sum.sum = 0
		for _, d := range data {
			sum.sum += d
		}
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (sum *Float64Sum) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (sum *Float64Sum) Initialize(f TickFunctor, _ []string) error {
	if _, ok := f.(Float64SliceObject); !ok {
		return fmt.Errorf("Float64Average is expecting a Float64SliceObject")
	}
	sum.f = f
	return nil
}

func (sum *Float64Sum) Float64() float64 {
	return sum.sum
}

func (sum *Float64Sum) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[%f]", sum.sum)), nil
}

type Float64GroupSum struct {
	m   map[uint64]TickFunctor
	f   TickFunctor
	sum float64
}

func (sum *Float64GroupSum) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	binary.LittleEndian.PutUint64(b, math.Float64bits(sum.sum))
	return b
}

func (sum *Float64GroupSum) FromSnapshot(b []byte) error {
	// TODO
	return nil
}

func (sum *Float64GroupSum) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64GroupSum).sum
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (sum *Float64GroupSum) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one
	return deltas[len(deltas)-1]
}

func (sum *Float64GroupSum) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	sum.sum = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (sum *Float64GroupSum) Clone() TickObject {
	return &Float64GroupSum{
		sum: sum.sum,
		f:   sum.f.Clone().(TickFunctor),
		m:   make(map[uint64]TickFunctor),
	}
}

func (sum *Float64GroupSum) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	if _, ok := sum.m[ID]; !ok {
		sum.m[ID] = sum.f.Clone().(TickFunctor)
	}
	deltas, err := sum.m[ID].ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	if deltas != nil {
		sum.sum = 0
		for _, f := range sum.m {
			sum.sum += f.(Float64Object).Float64()
		}
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (sum *Float64GroupSum) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := sum.m[ID].ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		sum.sum = 0
		for _, f := range sum.m {
			sum.sum += f.(Float64Object).Float64()
		}
		sumVal := sum.sum
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&sumVal),
			Len:     1,
		}, nil
	} else {
		return nil, nil
	}
}

func (avg *Float64GroupSum) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (sum *Float64GroupSum) Initialize(f TickFunctor, _ []string) error {
	if _, ok := f.(Float64Object); !ok {
		return fmt.Errorf("Float64GroupSum is expecting a Float64Object")
	}
	sum.m = make(map[uint64]TickFunctor)
	sum.f = f
	return nil
}

func (sum *Float64GroupSum) Float64() float64 {
	return sum.sum
}

func (sum *Float64GroupSum) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[%f]", sum.sum)), nil
}

type Float64Median struct {
	f   TickFunctor
	med float64
}

func (med *Float64Median) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	binary.LittleEndian.PutUint64(b, math.Float64bits(med.med))
	return b
}

func (med *Float64Median) FromSnapshot(b []byte) error {
	// TODO
	return nil
}

func (med *Float64Median) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64Median).med
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (med *Float64Median) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	med.med = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr((deltas.Len-1)*8)))
	return nil
}

func (med *Float64Median) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (med *Float64Median) Clone() TickObject {
	// TODO vals clone
	return &Float64Median{
		med: med.med,
		f:   med.f.Clone().(TickFunctor),
	}
}

func (med *Float64Median) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := med.f.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		data := med.f.(Float64SliceObject).Float64Slice()
		if len(data) > 0 {
			sort.Float64s(data)
			med.med = data[len(data)/2]
			val := med.med
			return &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}, nil
		} else {
			return nil, nil
		}
	} else {
		return nil, nil
	}
}

func (med *Float64Median) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := med.f.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		data := med.f.(Float64SliceObject).Float64Slice()
		if len(data) > 0 {
			sort.Float64s(data)
			med.med = data[len(data)/2]
			val := med.med
			return &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}, nil
		} else {
			return nil, nil
		}
	} else {
		return nil, nil
	}
}

func (med *Float64Median) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (med *Float64Median) Initialize(f TickFunctor, _ []string) error {
	if _, ok := f.(Float64SliceObject); !ok {
		return fmt.Errorf("Float64Median is expecting a Float64SliceObject")
	}
	med.f = f
	return nil
}

func (med *Float64Median) Float64() float64 {
	return med.med
}

func (med *Float64Median) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("[%f]", med.med)), nil
}

// A float64TickWindow is a window that contains all the data
// in a certain tick range
type Float64TickWindow struct {
	F        TickFunctor
	Data     []float64
	Ticks    []uint64
	End      int
	window   []float64
	size     uint64
	nextTick uint64
}

func (w *Float64TickWindow) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	buff := bytes.Buffer{}
	// write size
	binary.LittleEndian.PutUint64(b, uint64(w.End))
	buff.Write(b)
	for i := 0; i < w.End; i++ {
		binary.LittleEndian.PutUint64(b, w.Ticks[i])
		buff.Write(b)
		binary.LittleEndian.PutUint64(b, math.Float64bits(w.Data[i]))
		buff.Write(b)
	}
	return buff.Bytes()
}

func (w *Float64TickWindow) FromSnapshot(snapshot []byte) error {
	// TODO need to create N tick objects and unserialize each one of them
	// so take the underlying functor, clone it N times, then call FromSnapshot on it
	return nil
}

func (w *Float64TickWindow) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	// TODO need to compare each array, and send the difference..
	// not really efficient..
	return gotickfile.TickDeltas{}, nil
}

func (w *Float64TickWindow) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// Deltas are just windows, so return last one which is the entire window
	return deltas[len(deltas)-1]
}

func (w *Float64TickWindow) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	// TODO need to use the delta to change array content
	return nil
}

func (w *Float64TickWindow) Clone() TickObject {
	// TODO return a new slide with an array containing a clone of all the tickobjects
	nw := Float64TickWindow{
		F:        w.F.Clone().(TickFunctor),
		Data:     make([]float64, 1, 1),
		Ticks:    w.Ticks,
		End:      w.End,
		size:     w.size,
		nextTick: w.nextTick,
	}
	return &nw
}

func (w *Float64TickWindow) sample(tick uint64) *gotickfile.TickDeltas {
	var deltas []float64

	if w.nextTick < tick {
		w.nextTick = uint64((math.Floor(float64(tick)/float64(w.size)) + 1) * float64(w.size))
		deltas = make([]float64, w.End, w.End)
		for i := 0; i < w.End; i++ {
			deltas[i] = w.Data[i]
		}
		w.End = 0
		w.window = deltas
	}

	// sample
	data := w.F.(Float64Object).Float64()
	w.Data[w.End] = data
	w.End += 1
	if w.End >= len(w.Data) {
		tmp := make([]float64, len(w.Data)*2, len(w.Data)*2)
		for i := 0; i < w.End; i++ {
			tmp[i] = w.Data[i]
		}
		w.Data = tmp
	}

	if len(deltas) > 0 {
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&deltas[0]),
			Len:     1,
		}
	} else {
		return nil
	}
}

func (w *Float64TickWindow) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	_, err := w.F.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	res := w.sample(tick)

	return res, nil
}

func (w *Float64TickWindow) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	_, err := w.F.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	res := w.sample(tick)

	return res, nil
}

func (w *Float64TickWindow) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (w *Float64TickWindow) Initialize(f TickFunctor, args []string) error {
	// parse the window size in number of ticks
	size, err := strconv.ParseInt(args[0], 10, 64)
	if err != nil {
		return err
	}
	if _, ok := f.(Float64Object); !ok {
		return fmt.Errorf("Float64Slide is expecting a Float64Object")
	}

	w.F = f

	w.Data = make([]float64, 1, 1)
	w.End = 0
	w.size = uint64(size)
	w.nextTick = 0

	return nil
}

func (w *Float64TickWindow) Float64Slice() []float64 {
	return w.window
}

type Float64Slide struct {
	F     TickFunctor
	Data  []float64
	Idx   int
	ready bool
}

func (s *Float64Slide) ToSnapshot() []byte {
	buff := &bytes.Buffer{}
	_ = binary.Write(buff, binary.LittleEndian, &s.Data)
	return buff.Bytes()
}

func (s *Float64Slide) FromSnapshot(snapshot []byte) error {
	reader := bytes.NewReader(snapshot)
	_ = binary.Read(reader, binary.LittleEndian, &s.Data)
	return nil
}

func (s *Float64Slide) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	slide := object.(*Float64Slide)
	deltas := make([]float64, len(slide.Data), len(slide.Data))
	for i := 0; i < len(s.Data); i++ {
		deltas[i] = s.Data[i]
	}
	var ptr unsafe.Pointer
	if len(deltas) > 0 {
		ptr = unsafe.Pointer(&deltas[0])
	}
	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     len(deltas),
	}, nil
}

func (s *Float64Slide) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	return deltas[len(deltas)-1]
}

func (s *Float64Slide) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	for i := 0; i < deltas.Len; i++ {
		delta := *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(i)*8))
		s.Data[s.Idx] = delta
		s.Idx += 1
		s.Idx = s.Idx % len(s.Data)
	}
	return nil
}

func (s *Float64Slide) Clone() TickObject {
	var F TickFunctor
	if s.F != nil {
		F = s.F.Clone().(TickFunctor)
	}
	clone := &Float64Slide{
		F:    F,
		Data: make([]float64, len(s.Data), len(s.Data)),
		Idx:  s.Idx,
	}
	return clone
}

func (s *Float64Slide) sample() *gotickfile.TickDeltas {
	sample := s.F.(Float64Object).Float64()
	var deltas []float64
	s.Data[s.Idx] = sample
	s.Idx += 1
	s.Idx = s.Idx % len(s.Data)
	if s.ready {
		deltas = []float64{sample}
	} else {
		if s.Idx == 0 {
			s.ready = true
			deltas = make([]float64, len(s.Data), len(s.Data))
			for i := 0; i < len(s.Data); i++ {
				deltas[i] = s.Data[i]
			}
		} else {
		}
	}
	if len(deltas) > 0 {
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&deltas[0]),
			Len:     len(deltas),
		}
	} else {
		return nil
	}
}

func (s *Float64Slide) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	deltas, err := s.F.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	// Sample only if the underlying functor has changed
	if deltas != nil {
		// application produced deltas
		return s.sample(), nil
	} else {
		return nil, nil
	}
}

func (s *Float64Slide) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := s.F.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	// Sample only if the underlying functor has changed
	if udeltas != nil {
		// application produced deltas
		return s.sample(), nil
	} else {
		return nil, nil
	}
}

func (s *Float64Slide) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (s *Float64Slide) Initialize(f TickFunctor, args []string) error {
	size, err := strconv.ParseInt(args[0], 10, 64)
	if err != nil {
		return err
	}
	if _, ok := f.(Float64Object); !ok {
		return fmt.Errorf("Float64Slide is expecting a Float64Object")
	}

	s.F = f

	s.Data = make([]float64, size, size)
	s.Idx = 0
	return nil
}

type Float64LessThan struct {
	F   TickFunctor
	val float64
	max float64
}

func (lt *Float64LessThan) ToSnapshot() []byte {
	b := make([]byte, 8, 8)
	binary.LittleEndian.PutUint64(b, math.Float64bits(lt.val))
	return b
}

func (lt *Float64LessThan) FromSnapshot(b []byte) error {
	// TODO
	return nil
}

func (lt *Float64LessThan) DeltasTo(object TickObject) (gotickfile.TickDeltas, error) {
	val := object.(*Float64LessThan).val
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (lt *Float64LessThan) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (lt *Float64LessThan) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	// TODO need to use the delta to change array content
	return nil
}

func (lt *Float64LessThan) Clone() TickObject {
	// return the last one..
	return &Float64LessThan{
		F:   lt.F.Clone().(TickFunctor),
		val: lt.val,
	}
}

func (lt *Float64LessThan) sample() *gotickfile.TickDeltas {
	val := lt.F.(Float64Object).Float64()
	if val < lt.max {
		lt.val = val
		return &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		return nil
	}
}

func (lt *Float64LessThan) ApplySnapshot(snapshot []byte, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	deltas, err := lt.F.ApplySnapshot(snapshot, ID, tick)
	if err != nil {
		return nil, err
	}
	// Sample only if the underlying functor has changed
	if deltas != nil {
		// application produced deltas
		return lt.sample(), nil
	} else {
		return nil, nil
	}
}

func (lt *Float64LessThan) ApplyDeltas(deltas gotickfile.TickDeltas, ID, tick uint64) (*gotickfile.TickDeltas, error) {
	udeltas, err := lt.F.ApplyDeltas(deltas, ID, tick)
	if err != nil {
		return nil, err
	}
	// Sample only if the underlying functor has changed
	if udeltas != nil {
		// application produced deltas
		return lt.sample(), nil
	} else {
		return nil, nil
	}
}

func (lt *Float64LessThan) Progress(tick uint64) (*gotickfile.TickDeltas, error) {
	// TODO
	return nil, nil
}

func (lt *Float64LessThan) Initialize(f TickFunctor, args []string) error {
	max, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		return err
	}
	if _, ok := f.(Float64Object); !ok {
		return fmt.Errorf("Float64Slide is expecting a Float64Object")
	}

	lt.F = f
	lt.max = max
	return nil
}

/*
type DownSample struct {
	ticks  uint64
	deltas []interface{}
	F      TickFunctor
}

func (ds *DownSample) ToSnapshot() []byte {
	return ds.F.ToSnapshot()
}

func (ds *DownSample) FromSnapshot(snapshot []byte) error {
	// TODO need to create N tick objects and unserialize each one of them
	// so take the underlying functor, clone it N times, then call FromSnapshot on it
	return nil
}

func (ds *DownSample) DeltasTo(object TickObject) (interface{}, error) {
	// TODO need to compare each array, and send the difference..
	// not really efficient..
	return nil, nil
}

func (ds *DownSample) ProcessDeltas(deltas interface{}) error {
	// TODO need to use the delta to change array content
	return nil
}

func (ds *DownSample) Clone() TickObject {
	// TODO return a new slide with an array containing a clone of all the tickobjects
	return nil
}

func (ds *DownSample) ApplySnapshot(snapshot []byte, ID, tick uint64) (interface{}, error) {
	_, err := ds.F.ApplySnapshot(snapshot, ID, tick);
	if err != nil {
		return nil, err
	}
	// Sample if first occurence
	if ds.nextTick == 0 {
		return true, nil
	} else if tick >  ds.nextTick {
		ds.nextTick += ds.ticks
		return true, nil
	} else {
		// No sampling
		return nil, nil
	}
}

func (ds *DownSample) ApplyDeltas(deltas interface{}, ID, tick uint64) (interface{}, error) {
	_, err := ds.F.ApplyDeltas(deltas, ID, tick);
	if err != nil {
		return nil, err
	}
	if tick > ds.nextTick {
		// Apply the deltas, and return the deltas
		ds.nextTick += ds.ticks
	}
}

func (ds *DownSample) Initialize(f TickFunctor, args []string) error {
	ticks, err := strconv.ParseUint(args[0], 10, 64)
	if err != nil {
		return err
	}
	ds.F = f
	ds.ticks = ticks
	return nil
}

*/
