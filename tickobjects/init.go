package tickobjects

import "reflect"

func init() {
	if err := RegisterTickObject("Float64Slide", reflect.TypeOf(Float64Slide{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("UInt64Sum", reflect.TypeOf(UInt64Sum{}), reflect.TypeOf(uint64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64TickWindow", reflect.TypeOf(Float64TickWindow{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64Average", reflect.TypeOf(Float64Average{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64Sum", reflect.TypeOf(Float64Sum{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64GroupSum", reflect.TypeOf(Float64GroupSum{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64Median", reflect.TypeOf(Float64Median{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Float64WindowSum", reflect.TypeOf(Float64WindowSum{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("EMA", reflect.TypeOf(EMA{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Return", reflect.TypeOf(Return{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Log", reflect.TypeOf(Log{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Abs", reflect.TypeOf(Abs{}), reflect.TypeOf(float64(0))); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("Trade", reflect.TypeOf(Trade{}), reflect.TypeOf(TradeDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("OHLCV", reflect.TypeOf(OHLCV{}), reflect.TypeOf(OHLCVDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("AggOHLCV", reflect.TypeOf(OHLCV{}), reflect.TypeOf(OHLCVDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("OBLiquidity", reflect.TypeOf(OBLiquidity{}), reflect.TypeOf(OBLiquidityDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("AggOBLiquidity", reflect.TypeOf(OBLiquidity{}), reflect.TypeOf(OBLiquidityDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("NewLiquidation", reflect.TypeOf(NewLiquidation{}), reflect.TypeOf(NewLiquidationDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("EVMERC721Tracker", reflect.TypeOf(ERC721Tracker{}), reflect.TypeOf(ERC721TrackerDelta{})); err != nil {
		panic(err)
	}
	if err := RegisterTickObject("EVMERC20Tracker", reflect.TypeOf(ERC20Tracker{}), reflect.TypeOf(ERC20Delta{})); err != nil {
		panic(err)
	}
}
