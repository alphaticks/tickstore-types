package tickobjects

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"math/big"
	"reflect"
	"unsafe"
)

var ErrSizeLimit = errors.New("token transfer amount above current owner's size")
var ErrMissingOwner = errors.New("token owner missing")
var ErrInsufficientFunds = errors.New("insufficient funds")
var ErrMaxSize = errors.New("max token size reached for owner")

type ERC20Tracker struct {
	owners    map[[20]byte]*big.Int
	lastBlock uint64
}

type ERC20Delta struct {
	From  [20]byte
	To    [20]byte
	Size  [32]byte
	Block uint64
}

type ERC20DeltaPacked struct {
	Pack [10]uint64
}

type ERC20Token struct {
	Owner [20]byte
	Size  [32]byte
}

func NewERC20Tracker() *ERC20Tracker {
	return &ERC20Tracker{
		owners:    make(map[[20]byte]*big.Int),
		lastBlock: 0,
	}
}

func (t *ERC20Tracker) ToSnapshot() []byte {
	b := bytes.NewBuffer(nil)

	if err := binary.Write(b, binary.LittleEndian, t.lastBlock); err != nil {
		panic(err)
	}
	for owner, size := range t.owners {
		var token ERC20Token
		size.FillBytes(token.Size[:])
		token.Owner = owner
		if err := binary.Write(b, binary.LittleEndian, token); err != nil {
			panic(err)
		}
	}
	return b.Bytes()
}

func (t *ERC20Tracker) FromSnapshot(b []byte) error {
	if len(b) <= 0 {
		return fmt.Errorf("empty slice")
	}
	r := bytes.NewBuffer(b)

	if err := binary.Read(r, binary.LittleEndian, &t.lastBlock); err != nil {
		return fmt.Errorf("error decoding: %v", err)
	}

	t.owners = make(map[[20]byte]*big.Int)
	owners := (len(b) - 8) / 52
	for i := 0; i < owners; i++ {
		var token ERC20Token
		if err := binary.Read(r, binary.LittleEndian, &token); err != nil {
			return fmt.Errorf("error decoding: %v", err)
		}
		t.owners[token.Owner] = big.NewInt(0).SetBytes(token.Size[:])
	}

	return nil
}

func (t *ERC20Tracker) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	o := other.(*ERC20Tracker)
	return gotickfile.TickDeltas{}, t.Compare(o)
}

func (t *ERC20Tracker) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	size := reflect.TypeOf(ERC20Delta{}).Size()
	return CopyDeltas(size, deltas...)
}

func (t *ERC20Tracker) ProcessDeltas(delta gotickfile.TickDeltas) error {
	sz := reflect.TypeOf(ERC20Delta{}).Size()
	for i := 0; i < delta.Len; i++ {
		updt := *(*ERC20Delta)(unsafe.Pointer(uintptr(delta.Pointer) + uintptr(i)*sz))
		if bytes.Equal(updt.From[:], make([]byte, 20)) && bytes.Equal(updt.To[:], make([]byte, 20)) {
			// Checkpoint
			t.lastBlock = updt.Block
			continue
		}
		amount := big.NewInt(0).SetBytes(updt.Size[:])
		minting := bytes.Equal(updt.From[:], make([]byte, 20))
		if minting {
			// Minting
			if size, ok := t.owners[updt.To]; !ok {
				t.owners[updt.To] = amount
			} else {
				size.Add(size, amount)
			}
		} else {
			// Transfer
			amountFrom, ok := t.owners[updt.From]
			if !ok {
				return ErrMissingOwner
			}
			if amountFrom.Cmp(amount) < 0 {
				return ErrInsufficientFunds
			}
			if amountTo, ok := t.owners[updt.To]; !ok {
				t.owners[updt.To] = amount
				amountFrom.Sub(amountFrom, amount)
			} else {
				amountTo.Add(amountTo, amount)
				amountFrom.Sub(amountFrom, amount)
			}
		}
		if len(t.owners[updt.To].Bytes()) > 32 {
			return ErrSizeLimit
		}

		t.lastBlock = updt.Block
	}
	return nil
}

func (t *ERC20Tracker) Clone() TickObject {
	snapshot := t.ToSnapshot()
	other := &ERC20Tracker{}
	_ = other.FromSnapshot(snapshot)
	return other
}

func (t *ERC20Tracker) BlockNumber() uint64 {
	return t.lastBlock
}

func (t *ERC20Tracker) GetTracker() map[[20]byte]*big.Int {
	return t.owners
}

func (t *ERC20Tracker) Compare(o *ERC20Tracker) error {
	if t.lastBlock != o.lastBlock {
		return fmt.Errorf("different last block")
	}
	for owner1, size1 := range t.owners {
		if size2, ok := o.owners[owner1]; !ok {
			return fmt.Errorf("missing owner")
		} else if size2.Cmp(size1) != 0 {
			return fmt.Errorf("different balance")
		}
	}
	return nil
}

func NewERC20Transfer(from, to [20]byte, size *big.Int, block uint64) ERC20Delta {
	var s [32]byte
	size.FillBytes(s[:])
	return ERC20Delta{
		From:  from,
		To:    to,
		Size:  s,
		Block: block,
	}
}

func NewERC20Checkpoint(block uint64) ERC20Delta {
	return ERC20Delta{
		Block: block,
	}
}
