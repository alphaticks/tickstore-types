package tickobjects

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"reflect"
	"unsafe"
)

var ErrMintNotZero = errors.New("new token must come from zero address")
var ErrSenderNotOwner = errors.New("token sender isn't the owner")

type ERC721Tracker struct {
	tokens    map[[32]byte][20]byte
	lastBlock uint64
}

type ERC721TrackerDelta struct {
	From    [20]byte
	To      [20]byte
	TokenId [32]byte
	Block   uint64
}

type ERC721Token struct {
	Owner   [20]byte
	TokenID [32]byte
}

func (t *ERC721Tracker) ToSnapshot() []byte {
	b := bytes.NewBuffer(nil)

	if err := binary.Write(b, binary.LittleEndian, t.lastBlock); err != nil {
		panic(err)
	}
	for _, tok := range t.tokens {
		if err := binary.Write(b, binary.LittleEndian, tok); err != nil {
			panic(err)
		}
	}
	return b.Bytes()
}

func (t *ERC721Tracker) FromSnapshot(b []byte) error {
	if len(b) <= 0 {
		return fmt.Errorf("empty slice")
	}
	r := bytes.NewBuffer(b)

	if err := binary.Read(r, binary.LittleEndian, &t.lastBlock); err != nil {
		return fmt.Errorf("error decoding: %v", err)
	}

	nTokens := (len(b) - 8) / 52
	t.tokens = make(map[[32]byte][20]byte)
	for i := 0; i < nTokens; i++ {
		var token ERC721Token
		if err := binary.Read(r, binary.LittleEndian, &token); err != nil {
			return err
		}
		t.tokens[token.TokenID] = token.Owner
	}

	return nil
}

func (t *ERC721Tracker) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	o := other.(*ERC721Tracker)

	var dlts []ERC721TrackerDelta
	for k, owner1 := range o.tokens {
		owner2, ok := t.tokens[k]
		if !ok || owner1 != owner2 {
			// We don't have the coin or the owner are different
			dlts = append(dlts, ERC721TrackerDelta{
				From:    owner2,
				To:      owner1,
				TokenId: k,
				Block:   t.lastBlock,
			})
		}
	}
	if len(dlts) > 0 {
		return gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&dlts[0]),
			Len:     len(dlts),
		}, nil
	} else {
		return gotickfile.TickDeltas{
			Pointer: nil,
			Len:     0,
		}, nil
	}
}

func (t *ERC721Tracker) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	var ndeltas []ERC721TrackerDelta
	size := reflect.TypeOf(ERC721TrackerDelta{}).Size()
	for _, delta := range deltas {
		for i := 0; i < delta.Len; i++ {
			ndeltas = append(ndeltas, *(*ERC721TrackerDelta)(unsafe.Pointer(uintptr(delta.Pointer) + uintptr(i)*size)))
		}
	}

	var ptr unsafe.Pointer
	if len(ndeltas) > 0 {
		ptr = unsafe.Pointer(&ndeltas[0])
	}
	delta := gotickfile.TickDeltas{
		Len:     len(ndeltas),
		Pointer: ptr,
	}
	return delta
}

func (t *ERC721Tracker) ProcessDeltas(delta gotickfile.TickDeltas) error {
	size := reflect.TypeOf(ERC721TrackerDelta{}).Size()
	for i := 0; i < delta.Len; i++ {
		updt := *(*ERC721TrackerDelta)(unsafe.Pointer(uintptr(delta.Pointer) + uintptr(i)*size))
		if _, ok := t.tokens[updt.TokenId]; !ok {
			// New token, must come from 0 address
			if !bytes.Equal(updt.From[:], make([]byte, 20)) {
				return ErrMintNotZero
			}
		} else {
			prevOwner := t.tokens[updt.TokenId]
			if !bytes.Equal(prevOwner[:], updt.From[:]) {
				return ErrSenderNotOwner
			}
		}
		t.tokens[updt.TokenId] = updt.To
		t.lastBlock = updt.Block
	}
	return nil
}

func (t *ERC721Tracker) Clone() TickObject {
	snapshot := t.ToSnapshot()
	other := &ERC721Tracker{}
	_ = other.FromSnapshot(snapshot)
	return other
}

func (t *ERC721Tracker) Supply() int {
	return len(t.tokens)
}

func (t *ERC721Tracker) Owner(tokenId [32]byte) [20]byte {
	return t.tokens[tokenId]
}

func (t *ERC721Tracker) Owned(owner [20]byte) [][32]byte {
	var res [][32]byte
	for c, o := range t.tokens {
		if o == owner {
			res = append(res, c)
		}
	}
	return res
}
