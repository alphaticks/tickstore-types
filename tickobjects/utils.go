package tickobjects

import (
	"github.com/melaurent/gotickfile/v2"
	"unsafe"
)

func CopyDeltas(size uintptr, deltas ...gotickfile.TickDeltas) gotickfile.TickDeltas {
	length := 0
	for _, d := range deltas {
		length += d.Len
	}
	dlts := make([]byte, length*int(size))
	l := 0
	for _, d := range deltas {
		for i := 0; i < d.Len; i++ {
			for j := 0; j < int(size); j++ {
				dlts[l] = *(*byte)(unsafe.Pointer(uintptr(d.Pointer) + uintptr(i)*size + uintptr(j)))
				l += 1
			}
		}
	}
	var ptr unsafe.Pointer
	if len(dlts) > 0 {
		ptr = unsafe.Pointer(&dlts[0])
	}
	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     length,
	}
}
