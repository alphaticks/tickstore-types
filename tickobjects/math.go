package tickobjects

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/melaurent/gotickfile/v2"
	"math"
	"strconv"
	"unsafe"
)

type Return struct {
	ready    bool
	ret      float64
	freq     uint64
	nextTick uint64
	lastVal  float64
	F        TickFunctor
}

func (p *Return) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, p.ret)
	return w.Bytes()
}

func (p *Return) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &p.ret); err != nil {
		return err
	}
	return nil
}

func (p *Return) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	obj := other.(*Return)
	val := obj.ret
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (p *Return) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (p *Return) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	p.ret = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))

	return nil
}

func (p *Return) Clone() TickObject {
	var F TickFunctor
	if p.F != nil {
		F = p.F.Clone().(TickFunctor)
	}
	return &Return{
		freq:     p.freq,
		F:        F,
		nextTick: 0,
		ready:    false,
	}
}

// Functor part
func (p *Return) ApplySnapshot(snap []byte, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if p.F == nil {
		if err := p.FromSnapshot(snap); err != nil {
			return nil, err
		}
		val := p.ret
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		if p.ready && tick > p.nextTick {
			val := p.F.(Float64Object).Float64()
			if p.nextTick != 0 {
				p.ret = val / p.lastVal
				tmp := p.ret
				deltas = &gotickfile.TickDeltas{
					Pointer: unsafe.Pointer(&tmp),
					Len:     1,
				}
			}
			p.lastVal = val
			p.nextTick = ((tick / p.freq) + 1) * p.freq
		}
		_, err := p.F.ApplySnapshot(snap, ID, tick)
		if err != nil {
			return nil, err
		}
		p.ready = true
	}
	return
}

func (p *Return) ApplyDeltas(ndeltas gotickfile.TickDeltas, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if p.F == nil {
		if err := p.ProcessDeltas(ndeltas); err != nil {
			return nil, err
		}
		val := p.ret
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		if p.ready && tick > p.nextTick {
			val := p.F.(Float64Object).Float64()
			if p.nextTick != 0 {
				p.ret = val / p.lastVal
				tmp := p.ret
				deltas = &gotickfile.TickDeltas{
					Pointer: unsafe.Pointer(&tmp),
					Len:     1,
				}
			}
			p.lastVal = val
			p.nextTick = ((tick / p.freq) + 1) * p.freq
		}
		_, err := p.F.ApplyDeltas(ndeltas, ID, tick)
		if err != nil {
			return nil, err
		}
		p.ready = true
	}
	return
}

func (p *Return) Progress(tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if tick > p.nextTick {
		val := p.F.(Float64Object).Float64()
		if p.nextTick != 0 {
			p.ret = val / p.lastVal
			tmp := p.ret
			deltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&tmp),
				Len:     1,
			}
		}
		p.lastVal = val
		p.nextTick = ((tick / p.freq) + 1) * p.freq
	}
	return
}

func (p *Return) Initialize(functor TickFunctor, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("Return expects frequency argument")
	}
	_, ok := functor.(Float64Object)
	if !ok {
		return fmt.Errorf("Return expects a Float64Object")
	}

	p.F = functor
	val, err := strconv.ParseUint(args[0], 10, 64)
	if err != nil {
		return err
	}
	p.freq = val
	p.ready = false

	return nil
}

func (p *Return) Float64() float64 {
	return p.ret
}

type Log struct {
	val float64
	F   TickFunctor
}

func (l *Log) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, l.val)
	return w.Bytes()
}

func (l *Log) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &l.val); err != nil {
		return err
	}
	return nil
}

func (l *Log) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	obj := other.(*Log)
	val := obj.val
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (l *Log) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (l *Log) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	l.val = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))

	return nil
}

func (l *Log) Clone() TickObject {
	var obj TickFunctor
	if l.F != nil {
		obj = l.F.Clone().(TickFunctor)
	}
	return &Log{
		F: obj,
	}
}

// Functor part
func (l *Log) ApplySnapshot(snap []byte, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if l.F == nil {
		if err := l.FromSnapshot(snap); err != nil {
			return nil, err
		}
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		udeltas, err := l.F.ApplySnapshot(snap, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			l.val = math.Log(l.F.(Float64Object).Float64())
			val := l.val
			deltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	}
	return
}

func (l *Log) ApplyDeltas(ndeltas gotickfile.TickDeltas, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if l.F == nil {
		if err := l.ProcessDeltas(ndeltas); err != nil {
			return nil, err
		}
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		udeltas, err := l.F.ApplyDeltas(ndeltas, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			l.val = math.Log(l.F.(Float64Object).Float64())
			val := l.val
			deltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	}
	return
}

func (l *Log) Progress(tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	udeltas, err := l.F.Progress(tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		l.val = math.Log(l.F.(Float64Object).Float64())
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	}
	return
}

func (l *Log) Initialize(functor TickFunctor, args []string) error {
	_, ok := functor.(Float64Object)
	if !ok {
		return fmt.Errorf("log expects a Float64Object")
	}

	l.F = functor

	return nil
}

func (l *Log) Float64() float64 {
	return l.val
}

type Abs struct {
	val float64
	F   TickFunctor
}

func (l *Abs) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, l.val)
	return w.Bytes()
}

func (l *Abs) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)
	if err := binary.Read(r, binary.LittleEndian, &l.val); err != nil {
		return err
	}
	return nil
}

func (l *Abs) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	obj := other.(*Abs)
	val := obj.val
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&val),
		Len:     1,
	}, nil
}

func (l *Abs) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (l *Abs) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	if deltas.Len == 0 {
		return nil
	}
	l.val = *(*float64)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*8))

	return nil
}

func (l *Abs) Clone() TickObject {
	var obj TickFunctor
	if l.F != nil {
		obj = l.F.Clone().(TickFunctor)
	}
	return &Abs{
		F: obj,
	}
}

// Functor part
func (l *Abs) ApplySnapshot(snap []byte, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if l.F == nil {
		if err := l.FromSnapshot(snap); err != nil {
			return nil, err
		}
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		udeltas, err := l.F.ApplySnapshot(snap, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			l.val = math.Abs(l.F.(Float64Object).Float64())
			val := l.val
			deltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	}
	return
}

func (l *Abs) ApplyDeltas(ndeltas gotickfile.TickDeltas, ID, tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	if l.F == nil {
		if err := l.ProcessDeltas(ndeltas); err != nil {
			return nil, err
		}
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	} else {
		udeltas, err := l.F.ApplyDeltas(ndeltas, ID, tick)
		if err != nil {
			return nil, err
		}
		if udeltas != nil {
			l.val = math.Abs(l.F.(Float64Object).Float64())
			val := l.val
			deltas = &gotickfile.TickDeltas{
				Pointer: unsafe.Pointer(&val),
				Len:     1,
			}
		}
	}
	return
}

func (l *Abs) Progress(tick uint64) (deltas *gotickfile.TickDeltas, err error) {
	udeltas, err := l.F.Progress(tick)
	if err != nil {
		return nil, err
	}
	if udeltas != nil {
		l.val = math.Abs(l.F.(Float64Object).Float64())
		val := l.val
		deltas = &gotickfile.TickDeltas{
			Pointer: unsafe.Pointer(&val),
			Len:     1,
		}
	}
	return
}

func (l *Abs) Initialize(functor TickFunctor, args []string) error {
	_, ok := functor.(Float64Object)
	if !ok {
		return fmt.Errorf("abs expects a Float64Object")
	}

	l.F = functor

	return nil
}

func (l *Abs) Float64() float64 {
	return l.val
}
