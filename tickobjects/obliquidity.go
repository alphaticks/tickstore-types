package tickobjects

import (
	"bytes"
	"encoding/binary"
	"math"
	"reflect"
	"unsafe"

	"github.com/melaurent/gotickfile/v2"
)

type OBLiquidity struct {
	Map map[int64]float64
	Ts  float64
}

type OBLiquidityDelta struct {
	Quantity float64
	Key      int64
}

var OBLiquidityDeltaSize = reflect.TypeOf(OBLiquidityDelta{}).Size()

func (p *OBLiquidity) ToSnapshot() []byte {
	w := &bytes.Buffer{}
	_ = binary.Write(w, binary.LittleEndian, OBLiquidityDelta{
		Quantity: p.Ts,
		Key:      math.MaxInt64,
	})
	for k, v := range p.Map {
		_ = binary.Write(w, binary.LittleEndian, OBLiquidityDelta{
			Quantity: v,
			Key:      k,
		})
	}
	return w.Bytes()
}

func (p *OBLiquidity) FromSnapshot(b []byte) error {
	p.Map = make(map[int64]float64)
	r := bytes.NewReader(b)
	for r.Len() > 0 {
		var delta OBLiquidityDelta
		if err := binary.Read(r, binary.LittleEndian, &delta); err != nil {
			return err
		}
		if delta.Key == math.MaxInt64 {
			p.Ts = delta.Quantity
		} else {
			p.Map[delta.Key] = delta.Quantity
		}
	}
	return nil
}

// TODO test
func (p *OBLiquidity) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	obj := other.(*OBLiquidity)

	var deltas []OBLiquidityDelta
	deltas = append(deltas, OBLiquidityDelta{
		Quantity: obj.Ts,
		Key:      math.MaxInt64,
	})
	for k := range p.Map {
		if _, ok := obj.Map[k]; !ok {
			deltas = append(deltas, OBLiquidityDelta{
				Quantity: 0.,
				Key:      k,
			})
		}
	}

	for k, v := range obj.Map {
		deltas = append(deltas, OBLiquidityDelta{
			Quantity: v,
			Key:      k,
		})
	}

	var ptr unsafe.Pointer
	if len(deltas) > 0 {
		ptr = unsafe.Pointer(&deltas[0])
	}

	return gotickfile.TickDeltas{
		Pointer: ptr,
		Len:     len(deltas),
	}, nil
}

func (p *OBLiquidity) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	buff := bytes.Buffer{}
	retDeltas := gotickfile.TickDeltas{
		Pointer: nil,
		Len:     0,
	}
	for _, delta := range deltas {
		tmpBuff := make([]byte, int(OBLiquidityDeltaSize)*delta.Len)
		for i := 0; i < len(tmpBuff); i++ {
			tmpBuff[i] = *(*byte)(unsafe.Pointer(uintptr(delta.Pointer) + uintptr(i)))
		}
		buff.Write(tmpBuff)
		retDeltas.Len += delta.Len
	}
	b := buff.Bytes()
	var ptr unsafe.Pointer
	if len(b) > 0 {
		ptr = unsafe.Pointer(&b[0])
	}
	retDeltas.Pointer = ptr
	return retDeltas
}

func (p *OBLiquidity) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	for i := 0; i < deltas.Len; i++ {
		delta := *(*OBLiquidityDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(i)*OBLiquidityDeltaSize))
		if delta.Key == math.MaxInt64 {
			p.Ts = delta.Quantity
		} else {
			if delta.Quantity == 0. {
				delete(p.Map, delta.Key)
			} else {
				// p = k * ts
				// ts = p /
				p.Map[delta.Key] = delta.Quantity
			}
		}
	}
	return nil
}

func (p *OBLiquidity) Clone() TickObject {
	return &OBLiquidity{
		Map: make(map[int64]float64),
		Ts:  p.Ts,
	}
}
