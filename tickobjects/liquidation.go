package tickobjects

import (
	"bytes"
	"encoding/binary"
	"unsafe"

	"github.com/melaurent/gotickfile/v2"
)

type NewLiquidation struct {
	M        map[uint64]bool
	price    float64
	quantity float64
}

type NewLiquidationDelta struct {
	Price    float64
	Quantity float64
}

func (l *NewLiquidation) ToSnapshot() []byte {
	b := bytes.NewBuffer(nil)

	if err := binary.Write(b, binary.LittleEndian, l.price); err != nil {
		panic(err)
	}
	if err := binary.Write(b, binary.LittleEndian, l.quantity); err != nil {
		panic(err)
	}

	return b.Bytes()
}

func (l *NewLiquidation) FromSnapshot(b []byte) error {
	r := bytes.NewReader(b)

	if err := binary.Read(r, binary.LittleEndian, &l.price); err != nil {
		return err
	}
	if err := binary.Read(r, binary.LittleEndian, &l.quantity); err != nil {
		return err
	}

	return nil
}

func (l *NewLiquidation) DeltasTo(other TickObject) (gotickfile.TickDeltas, error) {
	otherl := other.(*NewLiquidation)
	delta := NewLiquidationDelta{
		Price:    otherl.price,
		Quantity: otherl.quantity,
	}
	return gotickfile.TickDeltas{
		Pointer: unsafe.Pointer(&delta),
		Len:     1,
	}, nil
}

func (l *NewLiquidation) AggregateDeltas(deltas []gotickfile.TickDeltas) gotickfile.TickDeltas {
	// return the last one..
	return deltas[len(deltas)-1]
}

func (l *NewLiquidation) ProcessDeltas(deltas gotickfile.TickDeltas) error {
	// You go through each deltas, and if the aggregate ID changes
	// then we have a new aggregated trade.
	delta := *(*NewLiquidationDelta)(unsafe.Pointer(uintptr(deltas.Pointer) + uintptr(deltas.Len-1)*TradeDeltaSize))
	l.price = delta.Price
	l.quantity = delta.Quantity

	return nil
}

func (l *NewLiquidation) Clone() TickObject {
	newl := &NewLiquidation{
		price:    l.price,
		quantity: l.quantity,
		M:        make(map[uint64]bool),
	}
	for k, v := range l.M {
		newl.M[k] = v
	}
	return newl
}

func (l *NewLiquidation) Price() float64 {
	return l.price
}

func (l *NewLiquidation) Size() float64 {
	return l.quantity
}
