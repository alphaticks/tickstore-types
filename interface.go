package tickstore_types

import (
	"github.com/melaurent/gotickfile/v2"
	"gitlab.com/alphaticks/tickstore-types/tickobjects"
	"reflect"
	"time"
)

type TickstoreClient interface {
	RegisterMeasurement(measurement string, typeID string) error
	DeleteMeasurement(measurement string, tags map[string]string, from, to uint64) error
	GetLastEventTime(measurement string, tags map[string]string) (uint64, error)
	NewTickWriter(measurement string, tags map[string]string, flushTime time.Duration) (TickstoreWriter, error)
	NewQuery(qs *QuerySettings) (TickstoreQuery, error)
}

type TickstoreWriter interface {
	WriteObject(tick uint64, object tickobjects.TickObject) error
	WriteDeltas(tick uint64, deltas gotickfile.TickDeltas) error
	Flush() error
	GetObject() tickobjects.TickObject
	Close() error
}

type TickstoreQuery interface {
	SetNextDeadline(time.Time)
	Next() bool
	Progress(end uint64) bool
	Read() (uint64, tickobjects.TickObject, uint64)
	ReadDeltas() (uint64, *gotickfile.TickDeltas, uint64)
	DeltaType() reflect.Type
	Tags() map[string]string
	Close() error
	Err() error
}

type DataContext interface {
	Start() // Query start
	Clear() // Refetch
	Apply(int64, int64, interface{}, uint64, map[string]string)
	DataSources() []DataSource
	Attach(sctx DataContext)
	Detach(sctx DataContext)
}

type DataSource struct {
	Name string
	Data func(ctx DataContext, idx int) interface{}
}

type DataEvent struct {
	Idx int
	Ctx DataContext
}
