package tickstore_types

import (
	"math"
	"time"
)

type Config func(s *QuerySettings)

func WithStreaming(streaming bool) Config {
	return func(qs *QuerySettings) {
		qs.WithStreaming(streaming)
	}
}

func WithBatchSize(batchSize uint64) Config {
	return func(qs *QuerySettings) {
		qs.BatchSize = batchSize
	}
}

func WithTimeout(timeout time.Duration) Config {
	return func(qs *QuerySettings) {
		qs.Timeout = uint64(timeout)
	}
}

func WithSampler(sampler interface{}) Config {
	return func(qs *QuerySettings) {
		qs.Sampler = sampler
	}
}

func WithSelector(selector string) Config {
	return func(qs *QuerySettings) {
		qs.Selector = selector
	}
}

func WithFrom(from uint64) Config {
	return func(qs *QuerySettings) {
		qs.From = from
	}
}

func WithTo(to uint64) Config {
	return func(qs *QuerySettings) {
		qs.To = to
	}
}

func WithObjectType(typ string) Config {
	return func(qs *QuerySettings) {
		qs.ObjectType = typ
	}
}

type QuerySettings struct {
	Streaming  bool
	BatchSize  uint64
	Timeout    uint64
	Selector   string
	From       uint64
	To         uint64
	Sampler    interface{}
	ObjectType string
}

func NewQuerySettings(configs ...Config) *QuerySettings {
	qs := &QuerySettings{
		Streaming: false,
		BatchSize: 1000,
		Timeout:   uint64(5 * time.Second),
		Selector:  "",
		From:      0,
		To:        math.MaxUint64,
		Sampler:   nil,
	}
	for _, c := range configs {
		c(qs)
	}
	return qs
}

func (qc *QuerySettings) WithObjectType(objectType string) {
	qc.ObjectType = objectType
}

func (qc *QuerySettings) WithStreaming(streaming bool) {
	qc.Streaming = streaming
}

func (qc *QuerySettings) WithSelector(selector string) {
	qc.Selector = selector
}

func (qc *QuerySettings) WithFrom(tick uint64) {
	qc.From = tick
}

func (qc *QuerySettings) WithTo(tick uint64) {
	qc.To = tick
}

type TickSampler struct {
	Interval uint64
}
